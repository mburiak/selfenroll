﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Colonial.SelfEnroll.Web.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{	
			ViewBag.DisplayNavigationBar = "True";
			ViewBag.DisplayBreadcrumbBanner = "True";
			ViewBag.DisplayCoreMenuOptions = "True";
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		public ActionResult Welcome()
		{
			ViewBag.DisplayNavigationBar = "True";
			ViewBag.DisplayBreadcrumbBanner = "False";
			ViewBag.DisplayCoreMenuOptions = "False";
			return View();
		}

        public ActionResult PersonDetails()
        {
            ViewBag.DisplayNavigationBar = "True";
            ViewBag.DisplayBreadcrumbBanner = "False";
            ViewBag.DisplayCoreMenuOptions = "False";
            return View();
        }

        public ActionResult PersonDetailsUpdated()
        {
            ViewBag.DisplayNavigationBar = "True";
            ViewBag.DisplayBreadcrumbBanner = "False";
            ViewBag.DisplayCoreMenuOptions = "False";
            return View();
        }

        public ActionResult Landing()
		{
			ViewBag.DisplayNavigationBar = "True";
			ViewBag.DisplayBreadcrumbBanner = "True";
			ViewBag.Breadcrumb = "Home";
			ViewBag.DisplayCoreMenuOptions = "True";
			return View();
		}

	    public ActionResult Enroll()
	    {
	        ViewBag.DisplayNavigationBar = "True";
	        ViewBag.DisplayBreadcrumbBanner = "True";
	        ViewBag.Breadcrumb = "Home > Short Term Disability";
	        ViewBag.DisplayCoreMenuOptions = "True";
	        return View();
	    }

        public ActionResult Login()
        {
            ViewBag.DisplayNavigationBar = "False";
            ViewBag.DisplayBreadcrumbBanner = "False";
            ViewBag.DisplayCoreMenuOptions = "False";
            return View();
        }

        [HttpPost]
        public void LoginUser(Colonial.SelfEnroll.Web.Models.UserLoginViewModel loginModel)
        {
            //log in
            Response.Redirect("Welcome");
        }

        [HttpPost]
        public void SavePersonDetails(Colonial.SelfEnroll.Web.Models.PersonDetailsViewModel personDetailsModel)
        {
            //save person details
            Response.Redirect("PersonDetailsUpdated");
        }

    }
}