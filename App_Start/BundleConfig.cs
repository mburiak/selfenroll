﻿using System.Web;
using System.Web.Optimization;

namespace Colonial.SelfEnroll.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/siteJs").Include(
                      "~/Scripts/portamento.js",
                      "~/Scripts/main.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/font-awesome.css",
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/style/site").Include(
                        "~/style/main.css",
                        "~/style/site.css"));

            bundles.Add(new StyleBundle("~/style/enroll").Include(
                        "~/style/enroll.css"));

            bundles.Add(new StyleBundle("~/style/login").Include(
                        "~/style/login.css"));

            bundles.Add(new StyleBundle("~/style/welcome").Include(
                        "~/style/welcome.css"));

            bundles.Add(new StyleBundle("~/style/person-details").Include(
                       "~/style/person-details.css"));

            bundles.Add(new StyleBundle("~/style/person-details-updated").Include(
                       "~/style/person-details-updated.css"));
        }
    }
}
