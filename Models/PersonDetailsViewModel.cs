﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace Colonial.SelfEnroll.Web.Models
{
    public class PersonDetailsViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Address")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        [Display(Name = "Date of birth")]
        public DateTime DateOfBirth { get; set; }
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Phone]
        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }
        [Display(Name = "Hours per week")]
        public decimal HoursPerWeek { get; set; }
        [Display(Name = "Hire date")]
        public DateTime DateOfHire { get; set; }
        [Display(Name = "Annual salary")]
        public decimal AnnualSalary { get; set; }
        [Display(Name = "Employee ID")]
        public string EmployeeId { get; set; }
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }
    }

}